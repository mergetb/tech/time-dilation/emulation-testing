# DMA Emulation Testing

This directory contains scripts for testing high-fidelity bus emulation between hosts. At the moment we are using the e1000e NIC inside VMs for testing. To get started

First set up the network.

```
./networking.sh
```

Then in two separate terminals (the scripts do not return) launch the VMs.

```
./a.sh
```

```
./b.sh
```

Note: **in order for the e1000e driver to be stable, you must load the kernel parameter `IntMode=1,1`** in the VMs.
