#!/bin/bash

set -e

#nic=e1000e
#nic=virtio-net-pci,tx=timer
nic=virtio-net-pci,bus_rate=10g,link_rate=10g

if [[ ! -f fedora-28.qcow2 ]]; then
  curl -L https://mirror.deterlab.net/rvn/img/fedora-28.qcow2 -o fedora-28.qcow2
fi

if [[ ! -f diskBbase.qcow2 ]]; then
  cp fedora-28.qcow2 diskBbase.qcow2
fi

rm -f diskB.qcow2
qemu-img create -f qcow2 \
  -o backing_file=diskBbase.qcow2 \
  diskB.qcow2

../qemu/x86_64-softmmu/qemu-system-x86_64 \
  -m 1024 \
  -accel kvm \
  -cpu host \
  -tdf 10 \
  -drive file=~/.steam-img/diskB.qcow2,index=0,media=disk \
  -netdev tap,id=net0,ifname=tapB,script=no \
  -device $nic,netdev=net0,id=net0,mac=52:54:00:a9:e1:bb \
  -kernel ../kvm/arch/x86_64/boot/bzImage \
  -nographic \
  -append "earlyprintk=serial console=tty1 console=ttyS0,115200n8 root=/dev/sda1 rw net.ifnames=0 biosdevname=0" 

  #-netdev user,id=hostnet0,net=192.168.76.0/24,dhcpstart=192.168.76.10 \
  #-device e1000,id=hostnet0,netdev=hostnet0 \
