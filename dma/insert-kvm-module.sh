#!/bin/bash

if [[ $UID != 0 ]]; then
  echo "must be root"
  exit 1
fi

cd /tmp/test/kvm

rmmod kvm_amd
rmmod kvm

insmod arch/x86/kvm/kvm.ko
insmod arch/x86/kvm/kvm-amd.ko
