#!/bin/bash

set -e
set -x

if [[ ! -d /proc/sys/net/ipv4/conf/qbr ]]; then
  sudo ip link add qbr type bridge
fi
if [[ ! -d /proc/sys/net/ipv4/conf/tapA ]]; then
  sudo ip tuntap add dev tapA mode tap user $(whoami)
fi
if [[ ! -d /proc/sys/net/ipv4/conf/tapB ]]; then
  sudo ip tuntap add dev tapB mode tap user $(whoami)
fi
sudo ip link set tapA master qbr
sudo ip link set tapB master qbr
sudo ip link set dev qbr up
sudo ip link set dev tapA up
sudo ip link set dev tapB up
