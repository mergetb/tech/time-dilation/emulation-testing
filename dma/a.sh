#!/bin/bash

set -e

#nic=e1000e
#nic=virtio-net-pci,tx=timer
nic=virtio-net-pci,bus_rate=10g,link_rate=10g

if [[ ! -f debian-buster-td.qcow2 ]]; then
  curl -L https://mirror.deterlab.net/rvn/img/debian-buster-td.qcow2 -o debian-buster-td.qcow2
fi

if [[ ! -f initrd.img-4.19.0-td+ ]]; then 
  curl -L https://mirror.deterlab.net/rvn/kernel/initrd.img-4.19.0-td+ -o initrd.img-4.19.0-td+
fi

if [[ ! -f diskAbase.qcow2 ]]; then
  cp debian-buster-td.qcow2 diskAbase.qcow2
fi

rm -f diskA.qcow2
qemu-img create -f qcow2 \
  -o backing_file=diskAbase.qcow2 \
  diskA.qcow2

#gdb --args \
../qemu/x86_64-softmmu/qemu-system-x86_64 \
  -machine q35 \
  -accel kvm \
  -cpu host \
  -cpu qemu64 \
  -m 1024 \
  -tdf 10 \
  -drive file=diskA.qcow2,index=0,if=none,id=drive0 \
  -device virtio-blk-pci,scsi=off,drive=drive0,id=disk0,bootindex=1 \
  -netdev tap,id=net0,ifname=tapA,script=no \
  -device $nic,netdev=net0,id=net0,mac=52:54:00:a9:e1:aa \
  -kernel ../kvm/arch/x86_64/boot/bzImage \
  -initrd initrd.img-4.19.0-td+ \
  -append "earlyprintk=serial console=tty1 console=ttyS0,115200n8 root=/dev/vda3 rw net.ifnames=0 biosdevname=0 tsc=reliable" \
  -nographic 

# -tdf 10 \
#-hda diskA.qcow2 \

# -netdev user,id=hostnet0,net=192.168.76.0/24,dhcpstart=192.168.76.9 \
# -device e1000,id=hostnet0,netdev=hostnet0 \
